import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
// import Logo from "../../assets/Logo.jpeg"

// import firebase from "firebase"

import {Link} from "react-router-dom";
import LogMenu from "./Menue"

import "./navbar.css";

import Drawer from "./Drawer"
import { StepLabel } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor:"#BC4242",
   },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  colorSecondary: {
    backgroundColor:"#BC4242"
  }
}));

export default function ButtonAppBar() {
  // let [show, setShow] = useState(Style.show)

  let [admin, setAdmin] = useState(<div>
    <Link style={{textDecoration:"none", fontWeight:"bold", color:"black" }} to="/login">Login
    </Link></div>);

  const classes = useStyles();

//     let login;
// firebase.auth().onAuthStateChanged(function(user) {
//     if (user) {
// setAdmin(
// <LogMenu/>) 
//    } 
// else {
//  setAdmin(<div style={{color:"black"}}>
// <Link style={{color:"BC4242"}} to="/login">ooooooooooooooooooooLogin</Link> ?????
//   </div>)
//       // No user is signed in.
//     }
//   });

//  useEffect(()=>{


//   firebase.auth().onAuthStateChanged(function(user) {
//     if (user) {
// setAdmin(
// <LogMenu/>) 
//    } });

  
//   },[]) 

  return (
    <div style={{backgroundColor:"#BC4242"}} className={classes.root}>
      <AppBar className="background" position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
<div className="showBig"><Drawer/>   </div>
       </IconButton>
<div className="showSmall" style={{display:"flex", flexWrap:"wrap", justifyContent:"space-between", margin:"20px"}}>
          {/* <img style={{maxWidth:"100px"}} src={Logo} alt="logo?"/>    */}
<Link style={{textDecoration:"none", color:"black" }} className="navLink" to="/" >Home </Link>
          <Link style={{textDecoration:"none", color:"black", paddingLeft:"20px" }} className="navLink"  to="/What-We-Do" > About us </Link>
          <Link style={{textDecoration:"none", color:"black", paddingLeft:"20px" }} className="navLink" to="/Advice" > Advice </Link>
          <Link style={{textDecoration:"none", color:"black", paddingLeft:"20px" }} className="navLink" to="/contact-us" > contact us </Link>
          </div>
 
          <Typography variant="h6" className={classes.title}>
       </Typography>
         {admin}

        </Toolbar>
      </AppBar>
    </div>
  );
}