import React from 'react';
// import clsx from 'clsx';
// import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import MenuIcon from '@material-ui/icons/Menu';
// import Style from "./navbar.module.scss"
import {Link} from "react-router-dom";

// const useStyles = makeStyles({
//   list: {
//     width: 250,
//   },
//   fullList: {
//     width: 'auto',
//   },
// });

export default function SwipeableTemporaryDrawer() {
//   const classes = useStyles();

  const [right, setRight] = React.useState(false)

  const toggleDrawer = (open) => (event) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
setRight(open)
  };

  const list = 
   <div>

       <div style={{display:"block",fontWeight:"bold", flexWrap:"wrap", justifyContent:"space-between", marginLeft:"20px", marginRight:"100px"}}>
       <br/>
          <Link style={{textDecoration:"none", color:"#0FBE7C" }} 
          // className={Style.drawerLink}
           to="/" >Home </Link>
          <br/>
          <br/>
          <Divider/>
          <br/>
          <Link style={{textDecoration:"none", color:"#0FBE7C" }} 
          // className={Style.drawerLink} 
           to="/What-We-Do" > What we do? </Link>
          <br/>
          <br/>
          <Divider/>
          <br/>
          <Link style={{textDecoration:"none", color:"#0FBE7C" }} 
          // className={Style.drawerLink}
           to="/Advice" > Advice </Link>
          <br/>
          <br/>
          <Divider/>
          <br/>
          <Link style={{textDecoration:"none", color:"#0FBE7C" }}
          //  className={Style.drawerLink}
            to="/contact-us" > contact us </Link>
          <br/>
          <br/>
         
          <Divider/>
         
          </div>

          <br/>

    </div>
  

  return (
    <div>

          <Button onClick={toggleDrawer(true)}> <MenuIcon />
</Button>
          <Drawer
            anchor='right'
            open={right}
            onClose={toggleDrawer(false)}
            onOpen={toggleDrawer(true)}
          >
            {list}
          </Drawer>
     
    </div>
  );
}
