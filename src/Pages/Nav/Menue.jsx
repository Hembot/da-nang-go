import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

// import firebase from "firebase"
import {Link} from "react-router-dom"


export default function SimpleMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  // const Logout = () => {
  //   const auth = firebase.auth();

  //   auth.signOut();   
  //     setAnchorEl(null);

  //  };


  return (
    <div >
      <Button style={{color:"#0FBE7C", textTransform:"capitalize", fontSize:"large", fontWeight:"bold"}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
Admin Menu   </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link style={{textDecoration:"none", color:"#0FBE7C"}} to="create-account">
               <MenuItem onClick={handleClose}>Create Account</MenuItem>
               </Link>

               <Link style={{textDecoration:"none", color:"#0FBE7C"}} to="Patients">
               <MenuItem onClick={handleClose}>Add Patients</MenuItem>
               </Link>

               <Link style={{textDecoration:"none", color:"#0FBE7C"}} to="list">
               <MenuItem onClick={handleClose}>View Patients</MenuItem>
               </Link>
               
        {/* <MenuItem style={{color:"pink", fontWeight:"bold"}} onClick={Logout}>Logout</MenuItem> */}
      </Menu>
    </div>
  );
}
