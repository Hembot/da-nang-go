import React, {useState, useEffect, useRef} from 'react'
import Nav from "../Nav/navBar"
// import firebase from "firebase"
import {Button} from "@material-ui/core"
import Paper from  "@material-ui/core/Paper"


function Advice() {

    let myRef = useRef();
    // let db = firebase.firestore();

    // let save = () => {
    //     db.collection("advice").doc("admin").set({
    //         page:myRef.current.innerHTML
    //     })
    // }

    const [body, setBody] = useState(` editable page, test it out to see changes. but nothing will be saved`);
    const [button, setButton] = useState("");
    const [editable, setEditable] = useState(true);

//     useEffect(()=>{
// db.collection("advice").doc("admin").get().then( doc => { 
//     if (doc.exists)
//     {
//         setBody(doc.data()
//         .page
//         .replace(/<div>|<\/div>/gi, "")
//         .replace(/<br>/gi, '\n') 
//         .replace(/&nbsp;/gi, '  ') 
//         );
// }
// })


//         firebase.auth().onAuthStateChanged(function(user) {
//             if (user) {
//               // User is signed in.
// setEditable(true);
// setButton(<div><Button variant="outlined" onClick={save}> save</Button></div>)
//             } else {
//               // No user is signed in.
// setEditable(false)

//             }
//           });

//     },[])


    return (
        <div>
            <Nav/>
            <br/>
            <br/>
            <br/>
            <Paper style={{margin:"0 auto",
            backgroundColor:"orange",
            borderRadius:"20px",
            maxWidth:"800px",
            
            //  padding:"20px",
             display:"flex", flexWrap:"wrap"}} >
                <div style={{margin:"0 auto"}}>
             <h1 style={{textAlign:"center", color:"black"}}> Advice</h1>
            <br/>

            <div ref={myRef} 
            suppressContentEditableWarning={true} 
            contentEditable={editable} style={{margin:"0 auto", whiteSpace:"pre"}}>
                {body}
            </div>
            {button}
            </div>


        </Paper>
        <br/>
        <br/>
        <br/>
        <br/>
        </div>
    )
}

export default Advice
