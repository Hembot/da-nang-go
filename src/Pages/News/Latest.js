import React from 'react'

function Latest() {
    return (
        <div style={{display:"flex"}}>
        <div style={{margin:"0 auto", maxWidth:"600px"}}>
            <div>
<div><strong>URGENT - EMERGENCY SUPPLY DONATIONS NEEDED</strong><span><img /></span></div>
    <br/>
<div>Hannah and the team are off on a post-Typhoon mission to Dai Loc this Sunday, 1st November.</div>
</div>
<div>
    <br/>
<div>Dai Loc is a rural district in Quang Nam, about 36km from Da Nang. This area was very 
    <br/>
    badly affected by Typhoon Molave. Currently the military are carrying out search and rescue 
    <br/>
    teams here for missing people.</div>
</div>
<div>
<div>We aim to deliver emergency supplies to 50 people in Dai Loc. Da Nang Go is collecting 
    <br/>
    donations of the following supplies:</div>
<div>- Dry food</div>
<div>- Milk</div>
<div>- Blankets</div>
<div>- Clothes (warm clothes, sandals, shoes)</div>
    <br/>
<div>- Medicine (tiger balm, bandages, hydrogen peroxide)</div>
<div>- Notebooks and pencils</div>
<div>- Money Donations</div>
</div>
<div>
    <br/>
<div>If you able to donate supplies please bring them to Hannah&rsquo;s Restaurant: 
    <br/>
    28 Ngo Thi Si, Da Nang.</div>
</div>
<div>
<div>For money donations please donate to the following accounts:</div>
</div>
<div>
<div>Vietnam Bank Account</div>
<div>Saccombank</div>
<div>nguyen ngoc huy</div>
<div>0400 7482 0672</div>
<div>Ref: Danang Go &amp; (your name)</div>
</div>
<div>
<div>Paypal: <span><a 
href="https://www.paypal.com/paypalme/iamjosaguiar?fbclid=IwAR3s7w-utg_94xCiJmjpt82Uk6L_EIRbaVcyHCQ2SAnP-PKC9yWk0A6FCaI"
 target="_blank" rel="noopener">https://www.paypal.com/paypalme/iamjosaguiar</a></span></div>
</div>
<div>
<div>Thank you so much for your continued support! <span><img /></span></div>
<div><span><a href="https://www.facebook.com/hashtag/vietnamfloods?__eep__=6&amp;__cft__[0]=AZUHO9YRHgoKW-C7zdF_RXCIhLSwlSB5N2MlN5INbgeuP-YFKA3n4zBBCgGWjxVwmW-8XfdlR8lzNRvE6fon4DC8HAGLfAs0PVhSCs-NPGjhos1IzeYLc7roSludE_MQ0ZDhweVmEaBxBkVjfU8yIlM8&amp;__tn__=*NK-R" target="_blank" rel="noopener">#vietnamfloods</a></span> <span><a href="https://www.facebook.com/hashtag/prayforvietnam?__eep__=6&amp;__cft__[0]=AZUHO9YRHgoKW-C7zdF_RXCIhLSwlSB5N2MlN5INbgeuP-YFKA3n4zBBCgGWjxVwmW-8XfdlR8lzNRvE6fon4DC8HAGLfAs0PVhSCs-NPGjhos1IzeYLc7roSludE_MQ0ZDhweVmEaBxBkVjfU8yIlM8&amp;__tn__=*NK-R" target="_blank" rel="noopener">#prayforvietnam</a></span> <span><a href="https://www.facebook.com/hashtag/vietnamflooding?__eep__=6&amp;__cft__[0]=AZUHO9YRHgoKW-C7zdF_RXCIhLSwlSB5N2MlN5INbgeuP-YFKA3n4zBBCgGWjxVwmW-8XfdlR8lzNRvE6fon4DC8HAGLfAs0PVhSCs-NPGjhos1IzeYLc7roSludE_MQ0ZDhweVmEaBxBkVjfU8yIlM8&amp;__tn__=*NK-R" target="_blank" rel="noopener">#vietnamflooding</a></span> <span><a href="https://www.facebook.com/hashtag/typhoonmolavevietnam?__eep__=6&amp;__cft__[0]=AZUHO9YRHgoKW-C7zdF_RXCIhLSwlSB5N2MlN5INbgeuP-YFKA3n4zBBCgGWjxVwmW-8XfdlR8lzNRvE6fon4DC8HAGLfAs0PVhSCs-NPGjhos1IzeYLc7roSludE_MQ0ZDhweVmEaBxBkVjfU8yIlM8&amp;__tn__=*NK-R" target="_blank" rel="noopener">#typhoonmolavevietnam</a></span></div>
</div>
        </div>
        </div>
    )
}

export default Latest
