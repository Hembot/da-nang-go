import React, {useState, useEffect, useRef} from 'react'
import Nav from "../Nav/navBar"
// import firebase from "firebase"
import LatestNews from "../News/Latest"
import Paper from "@material-ui/core/Paper";
import Many from "../../assets/many.jpg"
import House from "../../assets/house.jpg"

import Button from "@material-ui/core/Button"
// import DatePicker from "../ContactUs/MaterialPick"



function WhatDo() {

    let myRef = useRef();
    // let db = firebase.firestore();

    // let save = () => {
    //     db.collection("about").doc("admin").set({
    //         page:myRef.current.innerHTML
    //     })
    // }

    const [body, setBody] = useState(`  a firebase create-react-app serverside rendered websit by alex hemming.`);
    const [button, setButton] = useState("");
    const [editable, setEditable] = useState(false);

//     useEffect(()=>{

// db.collection("about").doc("admin").get().then( doc => { 
//     if (doc.exists)
//     {
//         setBody(doc.data()
//         .page
//         .replace(/<div>|<\/div>/gi, "")
//         .replace(/<br>/gi, '\n') 
//         );
    
// }
// })


//         firebase.auth().onAuthStateChanged(function(user) {
//             if (user) {
//               // User is signed in.
// setEditable(true);
// setButton(<div><Button variant="outlined" onClick={save}> save</Button></div>)
//             } else {
//               // No user is signed in.
// setEditable(false)

//             }
//           });

//     },[])


    return (
<div style={{backgroundColor:"#BC4242"}}>           
            <Nav/>
            <br/>
            <div style={{ display:"flex", flexWrap:"wrap"}}>
                <Paper elevate={3} style={{margin:"0 auto", borderRadius:"20px", backgroundColor:"orange", maxWidth:"700px", padding:"20px"}}>
            <h1 style={{textAlign:"center"}}> About Us</h1>

            {/* <div className="card z-depth-0 project-summery"> a charity</div>
            Da Nang Go  is a non-profit charitable organization focused on 
            philanthropic activities to benefit Vietnamese in need throughout Central Vietnam. 
            The organization uses fundraising events and direct donations to address the specific needs of Vietnamese victims of natural disasters, poverty, and hunger. Da Nang Go also works closely with other philanthropic organizations in Vietnam and abroad to organize and streamline the accumulation, packaging, and delivery of aid to areas most affected and in need. Da Nang Go provides many aid items including (but not limited to) food, clean water, medicines, clothes, school supplies, cash aid, feminine products, transportation, and various labor needs. Da Nang Go strives to address specific aid concerns with a quick response while working with local authorities to ensure safe and efficient delivery of donations/supplies. The Da Nang Go Organization is led by a board of directors who ultimately decide how and where aid is distributed. Da Nang Go strives for transparency and liquidity to develop trust and support in Vietnam and abroad. Most recently the organization raised and 
            delivered over $200,000,000 VND in aid for flood related victims across central Vietnam.
            <div ref={myRef} 
            //  onChange={onchange} 
            value={body}
            suppressContentEditableWarning={true} 
            style={{whiteSpace:"pre"}} 
            contentEditable={editable}
             >
                {body}<LatestNews/>

            </div>       */}
            <div style={{maxwidth:"600px", padding:"30px", fontSize:"19px", fontWeight:"540"}}>
                <div>
                    
                <img src={Many} 
                style={{maxWidth:"300px", float:"right", margin:"15px", borderRadius:"15px", border:"2px solid black"}}
                />
                Da Nang Go  is a non-profit charitable organization focused on philanthropic activities to 
                benefit Vietnamese in need throughout Central Vietnam. 
                
                <br/>
                The organization uses fundraising events
                 and direct donations to address the specific needs of Vietnamese victims of natural disasters,
                  poverty, and hunger. 
                  <br/>
                  
                  Da Nang Go also works closely with other philanthropic organizations in
                   Vietnam and abroad to organize and streamline the accumulation, packaging, and delivery of 
                   aid to areas most affected and in need. 
                   
                   <br/>                   <br/>

                   Da Nang Go provides many aid items including 
                   (but not limited to) food, clean water, medicines, clothes, school supplies, cash aid, 
                   feminine products, transportation, and various labor needs.
                   
                   <br/>
                    Da Nang Go strives to address 
                   specific aid concerns with a quick response while working with local authorities to ensure 
                   safe and efficient delivery of donations/supplies. 
                   <br/>
                   The Da Nang Go Organization is led by a 
                   board of directors who ultimately decide how and where aid is distributed. Da Nang Go strives 
                   for transparency and liquidity to develop trust and support in Vietnam and abroad.
                    Most recently the organization raised and delivered over $200,000,000 VND 
                    in aid for flood related victims across central Vietnam.</div>
          </div> 
          
           {/* <DatePicker/> */}
                {button}

                <img src={House}  
                style={{maxWidth:"300px", margin:"15px",borderRadius:"15px", border:"2px solid black"}}
                />
       </Paper>   
       </div>
      
        </div>
    )
}

export default WhatDo
