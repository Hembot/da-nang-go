import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

export default function DateAndTimePickers() {
  const classes = useStyles();
  let date = new Date()

  let [month, setMonth] = useState(date.getMonth());
  let [day, setDay] = useState(date.getDay());
  let [hour, sethour] = useState(date.getHours());
  let [input, setInput] = useState();
    let year =date.getFullYear();

  let [value, setValue] = useState(`${year}-${month}-${day}T${hour}:00`);
  useEffect(() => {

  if (month < 10){
      setMonth(`0${month}`)
  }
  if (day < 10){
    setDay(`0${day}`)
}
if (hour < 10){
    sethour(`0${hour}`)
}
setValue(`${year}-${month}-${day}T${hour}:00`)

// setInput(`${year}-${month}-${day}T${hour}:00`)


  },[])






  return (
      <div>
    <form className={classes.container} onSubmit={(e)=>{
        e.preventDefault();
        console.log(value)}} noValidate>
      <TextField
        id="datetime-local"
        label="Next appointment"
        type="datetime-local"
        // defaultValue={`${year}-${month}-${day}T${hour}:00`}
        defaultValue={value}
        // "2017-05-24T10:30"
        className={classes.textField}
        value={value}
        onChange={(e)=>{setValue(e.target.value)}}
        InputLabelProps={{
          shrink: true,
        }}
      />
      <button type="subnmit"> submit</button>
    </form>      <button onClick={()=> {console.log(`${year}-${month}-${day}T${hour}:00`)}}>click</button></div>

  );
}
