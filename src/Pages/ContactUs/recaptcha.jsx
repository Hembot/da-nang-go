import ReCAPTCHA from "react-google-recaptcha";
import React from "react"
 
 
const ReCAPTCHAForm = (props) => {
  const recaptchaRef = React.useRef();
 
  const onSubmitWithReCAPTCHA = async () => {
    const token = await recaptchaRef.current.executeAsync();
 
    // apply to form data
  }
  return (
    <form onSubmit={onSubmitWithReCAPTCHA}>
      <ReCAPTCHA
        ref={recaptchaRef}
        size="normal"
        sitekey="6LfPxrMZAAAAAFfPla63WpldComJAg68pnF3HN3w"
      />
    </form>
 );
 
}

export default ReCAPTCHAForm;