import React, { useEffect } from "react";
import {  Router, Route, Switch } from "react-router-dom";
import styled from "styled-components";

import Home from "./Pages/Home.js"
import Two from "./Pages/Two.js";
 
import Contact from "./pages/ContactUs/Contact2.jsx";
import Advice from "./pages/Advice/Advice";
import WhatDo from "./pages/WhatDo/WhatDo";

import Nav from "./Pages/Nav/navBar"

function AppRouter(props) {

  useEffect(()=> {
document.title = "Da Nang Go"
document.body.style.backgroundColor = "#BC4242"

  },[])
  // return (
  //   <Router history={history}>
  //   <Route
  //  render={({location}) => {
    return ( 
      <div style={{height:"100vh", position:"relative", top:"-5px", right:"10px", width:`calc(100% + 15px)`}}>
          <Switch>
          <Route path="/" exact component={Home} />    
          <Route path="/two" exact component={Two} /> 
          <Route path="/contact-us" exact component={Contact} />    
          <Route path="/Advice" exact component={Advice} />    
          <Route path="/What-We-DO" exact component={WhatDo} />    
          </Switch>
      </div>
            );
  // }}/>
  //   </Router>
  // );
    }
export default AppRouter;